Require Import Coq.Strings.String.

Definition my_string := ("Hello world!" % string).

Eval compute in my_string.
