# Kurak

Jakieś głupoty co żem w Coqu napisał bo mi się nudziło.

## About

Some [Coq](https://coq.inria.fr/) proof assistant experimentation.

## License

Unless otherwise stated, this code is released under the "CC0 1.0 Universal"
license (see the "LICENSE.txt" file) and comes with absolutely no warranty.
